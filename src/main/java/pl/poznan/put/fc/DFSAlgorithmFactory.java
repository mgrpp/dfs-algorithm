package pl.poznan.put.fc;

import pl.poznan.graph.api.Algorithm;
import pl.poznan.graph.api.AlgorithmFactory;

public class DFSAlgorithmFactory implements AlgorithmFactory{
    @Override
    public Algorithm createAlgorithmInstance() {
        return new DFSAlgorithm();
    }
}
