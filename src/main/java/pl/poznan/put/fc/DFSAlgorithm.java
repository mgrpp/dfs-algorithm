package pl.poznan.put.fc;

import pl.poznan.graph.api.*;
import pl.poznan.graph.api.variable.NodeVariable;
import pl.poznan.graph.api.variable.Variables;

import java.math.BigDecimal;
import java.util.*;

public class DFSAlgorithm implements Algorithm {

    public static final String TYPE = "type";
    public static final String MAX_ID = "max_id";

    @Override
    public void onCreate(Context context) throws Exception {
        context.getLogger().debug("Created new node: {0}", context.getCurrentNode().getId());
        reset(context);
    }

    @Override
    public List<Message> onChange(Context context) throws Exception {
        reset(context);
        return null;
    }

    private void reset(Context context) {
        DFSRegistry registry = context.getRegistry(DFSRegistry.class);
        context.getLogger().debug("Registry before clean for node: {0}. Registry value: {1}", context.getCurrentNode().getId(), registry);
        registry.setVisited(Variables.newHashSet(Node.class));
        registry.setSender(null);
        registry.setMaxId(context.getCurrentNode().getIdAsInt());
    }

    @Override
    public List<Message> onMessage(Context context) throws Exception {
        if (isNewIteration(context)) {
            return startIteration(context);
        }
        MessageContent msgContent = context.getMessage().getContent();
        DFSMessageType messageType = msgContent.getAttributeOf(TYPE, DFSMessageType.class);
        context.getLogger().info("Node {0} received message from {1}. Message type {2}",
                context.getCurrentNode().getId(),
                context.getSender().getId(),
                messageType);
        switch (messageType) {
            case VISIT:
                return performVisit(context);
            case MAX:
                return performMax(context);
            default:
                throw new IllegalArgumentException("Received unknown message type: " + messageType);
        }
    }

    private boolean isNewIteration(Context context) {
        return context.getMessage().getType() == MessageType.INIT;
    }

    private List<Message> startIteration(Context context) {
        Node unvisitedNode = getFirstUnvisitedNode(context);
        if (existsUnvisitedNode(unvisitedNode)) {
            DFSRegistry registry = context.getRegistry(DFSRegistry.class);
            registry.getVisited().add(unvisitedNode);
            return Arrays.asList(createVisitMessage(context, unvisitedNode));
        }
        context.getLogger().warn("Node {0} returned empty messages", context.getCurrentNode().getId());
        return Collections.emptyList();
    }

    private List<Message> performVisit(Context context) {
        DFSRegistry registry = context.getRegistry(DFSRegistry.class);
        registry.setSender(context.getSender());
        Node unvisited = getFirstUnvisitedNode(context);
        if (existsUnvisitedNode(unvisited)) {
            registry.getVisited().add(unvisited);
            return Arrays.asList(createVisitMessage(context, unvisited));
        } else {
            Node sender = registry.getSender().getAndReset();
            return Arrays.asList(createMaxMessage(context, sender, registry.getMaxId()));
        }
    }

    private List<Message> performMax(Context context) {
        int maxId = compareAndReturnGreaterId(context);
        DFSRegistry registry = context.getRegistry(DFSRegistry.class);
        registry.setMaxId(maxId);
        Node unvisited = getFirstUnvisitedNode(context);
        if (existsUnvisitedNode(unvisited)) {
            registry.getVisited().add(unvisited);
            return Arrays.asList(createVisitMessage(context, unvisited));
        } else {
            return returnMaxOrEndAlgorithm(context);
        }
    }

    private List<Message> returnMaxOrEndAlgorithm(Context context) {
        DFSRegistry registry = context.getRegistry(DFSRegistry.class);
        NodeVariable variable = registry.getSender();
        if (variable.isNull()) {
            context.getStatistic().put("Max id", new BigDecimal(registry.getMaxId()));
            return Collections.emptyList();
        }
        Node sender = variable.getAndReset();
        return Arrays.asList(createMaxMessage(context, sender, registry.getMaxId()));
    }


    private int compareAndReturnGreaterId(Context context) {
        DFSRegistry registry = context.getRegistry(DFSRegistry.class);
        MessageContent msgContent = context.getMessage().getContent();
        int maxIdFromMessage = msgContent.getIntegerAttribute(MAX_ID);
        return maxIdFromMessage > registry.getMaxId() ? maxIdFromMessage : registry.getMaxId();
    }

    private Node getFirstUnvisitedNode(Context context) {
        Set<Node> outgoingNodes = new HashSet<>();
        NetworkNode currentNode = context.getCurrentNode();
        outgoingNodes.addAll(currentNode.getOutgoingNodes());
        DFSRegistry registry = context.getRegistry(DFSRegistry.class);
        outgoingNodes.removeAll(registry.getVisited());
        Iterator<Node> it = outgoingNodes.iterator();
        return it.hasNext() ? it.next() : null;
    }

    private boolean existsUnvisitedNode(Node node) {
        return Objects.nonNull(node);
    }

    private Message createVisitMessage(Context context, Node node) {
        context.getStatistic().put("Visit count", BigDecimal.ONE, (oldValue, newValue) -> oldValue.add(newValue));
        return MessageBuilder
                .builder()
                .addAttribute(TYPE, DFSMessageType.VISIT)
                .withReceiver(node)
                .build();
    }

    private Message createMaxMessage(Context context, Node node, int max) {
        context.getStatistic().put("Max count", BigDecimal.ONE, (oldValue, newValue) -> oldValue.add(newValue));
        return MessageBuilder
                .builder()
                .addAttribute(TYPE, DFSMessageType.MAX)
                .addAttribute(MAX_ID, max)
                .withReceiver(node)
                .build();
    }
}
