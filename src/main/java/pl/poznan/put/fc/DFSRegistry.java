package pl.poznan.put.fc;

import pl.poznan.graph.api.Node;
import pl.poznan.graph.api.variable.NodeVariable;

import java.util.Set;

public interface DFSRegistry {

    void setVisited(Set<Node> nodes);

    Set<Node> getVisited();

    void setSender(Node node);

    NodeVariable getSender();

    void setMaxId(int id);

    int getMaxId();
}
